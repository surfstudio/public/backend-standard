package ru.surfstudio.controllers


import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import org.springframework.http.HttpStatus.*
import java.time.LocalDateTime

@ControllerAdvice
class ControllerAdvice() : ResponseEntityExceptionHandler() {

    @ExceptionHandler(Exception::class)
    fun defaultExceptionHandler(ex: Exception): ResponseEntity<ErrorResponseDto> {
        logger.error("Internal server error", ex)
        return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(ErrorResponseDto(message = ex.message, code = INTERNAL_SERVER_ERROR.value()))
    }

}

data class ErrorResponseDto(val message: String?, val code: Int, val timestamp: LocalDateTime = LocalDateTime.now())
