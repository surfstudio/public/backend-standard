@Library('surf-lib@backend')
import ru.surfstudio.ci.pipeline.tag.TagPipelineBackend

def pipeline = new TagPipelineBackend(this)
pipeline.init()
pipeline.dockerImageForBuild = "gradle:6.0.1-jdk11"
pipeline.registryPathAndProjectId = "surf-infrastructure/template/template"
pipeline.run()