package ru.surfstudio

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.openfeign.EnableFeignClients
import springfox.documentation.swagger2.annotations.EnableSwagger2

@SpringBootApplication
@EnableFeignClients
@EnableSwagger2
class TemplateApplication

fun main(args: Array<String>) {
	runApplication <TemplateApplication>(*args)
}