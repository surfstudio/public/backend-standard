package ru.surfstudio.controllers

import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/example")
class DefaultController {
    companion object {
        val logger = LoggerFactory.getLogger(DefaultController::class.java)
    }

    @GetMapping
    fun getIt(@RequestParam id: String): ResponseEntity<Any>{
        logger.info("id is $id")
        if(id.isEmpty()) throw Error("Something is wrong")
        return ResponseEntity.status(HttpStatus.OK).body(mapOf("id" to id))
    }
}