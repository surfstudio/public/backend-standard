package ru.surfstudio.configs.interceptors

val USER_ID = "user.id"
val REQUEST_ID = "req.id"
val REMOTE_HOST = "req.remoteHost"
val REQUEST_URI = "req.requestURI"
val REQUEST_URL = "req.requestURL"
val REQUEST_METHOD = "req.method"
val REQUEST_QUERY = "req.queryString"
val REQUEST_USER_AGENT = "req.userAgent"
val REQUEST_IP = "req.ip"