package ru.surfstudio.configs.interceptors

import org.slf4j.MDC
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.stereotype.Component
import org.springframework.web.servlet.HandlerInterceptor
import org.springframework.web.servlet.ModelAndView
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
@ConditionalOnProperty(prefix = "mdc", value = ["enabled"], havingValue = "true", matchIfMissing = false)
class WebMdcInterceptor : HandlerInterceptor {
    override fun preHandle(request: HttpServletRequest, response: HttpServletResponse, handler: Any): Boolean {
        initMdc(request)
        return true
    }

    private fun initMdc(request: HttpServletRequest) {
        MDC.put(REQUEST_ID, UUID.randomUUID().toString())
        MDC.put(REMOTE_HOST, request.remoteHost);
        MDC.put(REQUEST_URI, request.requestURI);
        MDC.put(REQUEST_URL, request.requestURL.toString());
        MDC.put(REQUEST_METHOD, request.method);
        MDC.put(REQUEST_QUERY, request.queryString);
        MDC.put(REQUEST_USER_AGENT, request.getHeader("User-Agent"));
        MDC.put(REQUEST_IP, request.getHeader("X-Forwarded-For"));
    }

    override fun postHandle(request: HttpServletRequest, response: HttpServletResponse, handler: Any, modelAndView: ModelAndView?) {
        MDC.clear()
    }
}
