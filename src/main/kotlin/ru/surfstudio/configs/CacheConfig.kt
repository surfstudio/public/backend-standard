package ru.surfstudio.configs

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.cache.annotation.EnableCaching
import org.springframework.context.annotation.Configuration
import javax.annotation.PostConstruct

@Configuration
@EnableCaching
class CacheConfig(@Value("\${spring.cache.type:\"\"}") val CACHE_TYPE: String) {

    companion object {
        private val logger: Logger = LoggerFactory.getLogger(CacheConfig::class.java)
    }
    @PostConstruct
    fun init() {
        if (CACHE_TYPE.toLowerCase().equals("none") || CACHE_TYPE.length == 0)
            logger.info("Cache is disabled")
        else
            logger.info("Cache is enabled: Type: $CACHE_TYPE")
    }
}