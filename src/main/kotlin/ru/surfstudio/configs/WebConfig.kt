package ru.surfstudio.configs

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.HandlerInterceptor
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import javax.annotation.PostConstruct

@Configuration
@ConditionalOnProperty(prefix = "mdc.request-id", value = ["enabled"], havingValue = "true", matchIfMissing = false)
class WebConfig(val interceptors: List<HandlerInterceptor>) : WebMvcConfigurer {
    override fun addInterceptors(registry: InterceptorRegistry) {
        for (interceptor in interceptors) {
            registry.addInterceptor(interceptor)
        }
        super.addInterceptors(registry)
    }
}