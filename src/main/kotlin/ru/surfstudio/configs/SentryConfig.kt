package ru.surfstudio.configs

import io.sentry.Sentry
import io.sentry.spring.SentryExceptionResolver
import io.sentry.spring.SentryServletContextInitializer
import org.slf4j.LoggerFactory
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.info.BuildProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.DependsOn
import org.springframework.scheduling.annotation.AsyncConfigurer
import org.springframework.scheduling.annotation.EnableAsync
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.annotation.SchedulingConfigurer
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler
import org.springframework.scheduling.config.ScheduledTaskRegistrar
import org.springframework.scheduling.support.TaskUtils
import org.springframework.stereotype.Component
import org.springframework.web.servlet.HandlerExceptionResolver
import org.springframework.web.servlet.ModelAndView
import java.lang.reflect.Method
import javax.annotation.PostConstruct
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


@Configuration("sentryInit")
@ConditionalOnProperty(prefix = "sentry", value = ["enabled"], havingValue = "true", matchIfMissing = false)
internal class SentryConfig(@Value("\${sentry.dsn:\"\"}") val sentryDsn: String) {
    @PostConstruct
    fun init() {
        Sentry.init(sentryDsn);
    }

    @Bean
    fun sentryExceptionResolver(): HandlerExceptionResolver {
        return SentryExceptionResolver()
    }

    @Bean
    fun sentryServletContextInitializer(): SentryServletContextInitializer {
        return SentryServletContextInitializer()
    }
}

@Component
internal class SentryExceptionResolverWithTags(val buildProperties: BuildProperties) : SentryExceptionResolver() {
    private val APP_VERSION_TAG = "APP_VERSION_TAG"
    private val appVersion = buildProperties.version ?: "not set"

    override fun resolveException(request: HttpServletRequest, response: HttpServletResponse, handler: Any?, ex: Exception): ModelAndView? {
        Sentry.getContext().addTag(APP_VERSION_TAG, appVersion)
        return super.resolveException(request, response, handler, ex)
    }
}

@EnableScheduling
@DependsOn("sentryInit")
@Configuration
@ConditionalOnProperty(prefix = "sentry", value = ["enabled"], havingValue = "true", matchIfMissing = false)
internal class SchedulingConfiguration(val buildProperties: BuildProperties) : SchedulingConfigurer {
    private val APP_VERSION_TAG = "APP_VERSION_TAG"
    private val appVersion = buildProperties.version ?: "not set"
    private val taskScheduler: ThreadPoolTaskScheduler = ThreadPoolTaskScheduler()

    init {
        val standardHandler = TaskUtils.LOG_AND_SUPPRESS_ERROR_HANDLER
        taskScheduler.setErrorHandler { t ->
            Sentry.getContext().addTag(APP_VERSION_TAG, appVersion)
            Sentry.capture(t)
            standardHandler.handleError(t)
        }
        taskScheduler.setThreadNamePrefix("@scheduled-")
        taskScheduler.initialize()
    }
    override fun configureTasks(taskRegistrar: ScheduledTaskRegistrar) {
        taskRegistrar.setScheduler(taskScheduler)
    }
}

@Configuration
@DependsOn("sentryInit")
@EnableAsync
@ConditionalOnProperty(prefix = "sentry", value = ["enabled"], havingValue = "true", matchIfMissing = false)
class AsyncConfig : AsyncConfigurer {
    override fun getAsyncUncaughtExceptionHandler(): AsyncUncaughtExceptionHandler? {
        return AsyncExceptionHandler()
    }
}

class AsyncExceptionHandler : AsyncUncaughtExceptionHandler {
    var logger = LoggerFactory.getLogger(AsyncExceptionHandler::class.java)
    override fun handleUncaughtException(
            throwable: Throwable, method: Method, vararg obj: Any) {
        logger.warn("Exception message - " + throwable.message)
        logger.warn("Method name - " + method.getName())
        for (param in obj) {
            logger.warn("Parameter value - $param")
        }
        Sentry.capture(throwable)
    }
}
