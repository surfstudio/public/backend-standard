package ru.surfstudio

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.should
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.startWith

class `Простой Юнит Тест` : StringSpec({
	"Проверка длины стринга" {
		"qwert".length shouldBe 5
	}
	"Проверка префикса" {
		"привет" should startWith("прив")
	}
})