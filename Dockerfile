FROM openjdk:11.0.4-jdk-slim

ENV TZ=Europe/Moscow
ENV START_SLEEP 0
ENV JAVA_OPTS ""

COPY ./build/libs/template*.jar /app/app.jar
RUN useradd -ms /bin/bash surf
RUN chown -R surf:surf /app
USER surf

EXPOSE 8080
EXPOSE 8081

CMD echo "The application will start in ${START_SLEEP}s..." && \
    sleep ${START_SLEEP} && \
    java ${JAVA_OPTS} -jar /app/app.jar
